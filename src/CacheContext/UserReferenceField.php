<?php

namespace Drupal\user_ref_field_cache_context\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\Context\UserCacheContextBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the QueryArgsCacheContext service, for "per query args" caching.
 *
 * Cache context ID: 'user.ref_field' (to vary by a reference field value).
 * Calculated cache context ID: 'user.ref_field:%key', e.g.'user.ref_field:foo'
 * (to vary by the 'foo' user field's value).
 */
class UserReferenceField extends UserCacheContextBase implements CalculatedCacheContextInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a UserReferenceField object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('User reference field');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($field = NULL) {
    if (empty($field)) {
      return '';
    }
    $current_user_id = $this->currentUser->id();
    // Load user object.
    $user = $this->entityTypeManager->getStorage('user')->load($current_user_id);
    if (!$user->hasField($field)) {
      return '?valueless?';
    }
    $field_val = $user->get($field);
    $value = $field_val->target_id ?? '?valueless?';
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($parameter = NULL) {
    return new CacheableMetadata();
  }

}
