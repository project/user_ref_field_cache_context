CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module implements a new cache context, based on the value of a reference
field on the current user. To use this new cache context in your own module,
after installing this module add a cache content like `user.ref_field:field_foo`
where `field_foo` is the machine name of the reference field (e.g. taxonomy
reference) whose value on the current user should alter what is being cached.

This module won't really do much for your website on its own, but can be useful
in your own custom module, or in another contrib module that requires it.


INSTALLATION
------------

 * Install this module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * There is no configuration, as this is really a helper module

MAINTAINERS
-----------

 * Current Maintainer: Martin Anderson-Clutz (mandclu) - https://www.drupal.org/u/mandclu
